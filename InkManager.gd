class_name InkManager
extends Node

@export_file("*.ink") var inkFile : set = set_story 

signal continued(data:Dictionary)

var story:Resource = null

func _ready():
	assert(story != null)
	var tmp = load("res://icon.svg")
	var methodMap = {}
	for i in story.get_method_list():
		if tmp.has_method(i.name):
			pass
		else:
			methodMap[i.name] = i

	var propertyMap = {}
	for i in story.get_property_list():
		propertyMap[i.name] = i
	for i in tmp.get_property_list():
		propertyMap.erase(i.name)
	pass


func set_story(value):
	inkFile = value	
	story = load(inkFile)
	story.Continued.connect(_continued)
	for i in story.GetVariableList():
		story.ObserveVariable(i,_variable_changed)

func chose_path_string(string:String,resetCallstack=true):
	story.ChoosePathString(string,resetCallstack)

func continue_story(): #todo return state
	story.Continue()


func _continued():
	var state = {
		text = story.GetCurrentText(),
		tags = story.GetCurrentTags(),
		choices = get_choices(),
		canContinue = story.GetCanContinue(),
	}
	
	continued.emit(state)

func get_choices():
	var choices = []
	for i in story.GetCurrentChoices():
		choices.push_back({
			text = i.GetText(),
			index = i.GetIndex(),
			tags = i.GetTags()
		})
	return choices

func select_choice(index:int):
	story.ChooseChoiceIndex(index)

signal variable_changed(name:String,value)

func _variable_changed(name:String,value):
	print('"%s" changed to: ' % [name] ,value)
	variable_changed.emit(name,value)


func onClick():
	ping.rpc()

@rpc("any_peer","reliable","call_local")
func ping():
	$PingAnimator.play()

